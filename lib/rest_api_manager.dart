import 'package:http/http.dart' as http;
import 'policy_model.dart';

//
class RestApiManager {
  String url = 'https://your-matrix-server.org/_matrix/corporal/policy';
  String bearer = 'Bearer YOUR_HTTP_API_TOKEN';

  Future<Corporal> loadPolicy() async {
    var headers = {'Authorization': bearer};
    var response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      final corporal = corporalFromJson(response.body);
      return corporal;
    } else {
      throw Exception('http.get error: statusCode= ${response.statusCode}');
    }
  }
}
