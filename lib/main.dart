import 'package:flutter/material.dart';
import 'package:corporal_ui/user_details.dart';
import 'package:corporal_ui/policy_model.dart';
import 'package:corporal_ui/rest_api_manager.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String searchString = "";
  TextEditingController searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var futureBuilder = new FutureBuilder<Corporal>(
      future: RestApiManager().loadPolicy(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            return Center(
              child: CircularProgressIndicator(),
            );
          default:
            if (snapshot.hasError)
              return new Text('Error: ${snapshot.error}');
            else
              return createListView(context, snapshot);
        }
      },
    );

    return Scaffold(
      appBar: AppBar(
        title: TextField(
          onChanged: (value) {
            setState(() {
              searchString = value;
            });
          },
          controller: searchController,
          decoration: InputDecoration(
            prefixIcon: Icon(Icons.search),
          ),
        ),
      ),
      body: futureBuilder,
    );
  }

  Widget createListView(BuildContext context, AsyncSnapshot snapshot) {
    return new ListView.builder(
        itemCount: snapshot.data.policy.users == null
            ? 0
            : snapshot.data.policy.users.length,
        itemBuilder: (BuildContext context, int index) {
          return snapshot.data.policy.users[index].displayName
                  .contains(searchString)
              ? Card(
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 32, bottom: 32, left: 16, right: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => UserDetails()));
                              },
                              child: Text(
                                snapshot.data.policy.users[index].displayName,
                                //'Note Title',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 22),
                              ),
                            ),
                            Text(
                              snapshot.data.policy.users[index].id,
                              //'Note Text',
                              style: TextStyle(color: Colors.grey.shade600),
                            ),
                          ],
                        ),
                        //SizedBox(width: 20),
                      ],
                    ),
                  ),
                )
              : Container();
        });
  }
}
