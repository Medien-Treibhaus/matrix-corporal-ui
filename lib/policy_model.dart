// Parsed with: https://app.quicktype.io/
// To parse this JSON data, do
//
//     final corporal = corporalFromJson(jsonString);

import 'dart:convert' show utf8;
import 'dart:convert';

Corporal corporalFromJson(String str) =>
    Corporal.fromJson(json.decode(utf8.decode(str.runes.toList())));

String corporalToJson(Corporal data) => json.encode(data.toJson());

class Corporal {
  Corporal({
    this.policy,
  });

  Policy policy;

  factory Corporal.fromJson(Map<String, dynamic> json) => Corporal(
        policy: Policy.fromJson(json["policy"]),
      );

  Map<String, dynamic> toJson() => {
        "policy": policy.toJson(),
      };
}

class Policy {
  Policy({
    this.schemaVersion,
    this.identificationStamp,
    this.flags,
    this.managedCommunityIds,
    this.managedRoomIds,
    this.users,
  });

  int schemaVersion;
  dynamic identificationStamp;
  Flags flags;
  List<String> managedCommunityIds;
  List<String> managedRoomIds;
  List<User> users;

  factory Policy.fromJson(Map<String, dynamic> json) => Policy(
        schemaVersion: json["schemaVersion"],
        identificationStamp: json["identificationStamp"],
        flags: Flags.fromJson(json["flags"]),
        managedCommunityIds:
            List<String>.from(json["managedCommunityIds"].map((x) => x)),
        managedRoomIds: List<String>.from(json["managedRoomIds"].map((x) => x)),
        users: List<User>.from(json["users"].map((x) => User.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "schemaVersion": schemaVersion,
        "identificationStamp": identificationStamp,
        "flags": flags.toJson(),
        "managedCommunityIds":
            List<dynamic>.from(managedCommunityIds.map((x) => x)),
        "managedRoomIds": List<dynamic>.from(managedRoomIds.map((x) => x)),
        "users": List<dynamic>.from(users.map((x) => x.toJson())),
      };
}

class Flags {
  Flags({
    this.allowCustomUserDisplayNames,
    this.allowCustomUserAvatars,
    this.forbidRoomCreation,
  });

  bool allowCustomUserDisplayNames;
  bool allowCustomUserAvatars;
  bool forbidRoomCreation;

  factory Flags.fromJson(Map<String, dynamic> json) => Flags(
        allowCustomUserDisplayNames: json["allowCustomUserDisplayNames"],
        allowCustomUserAvatars: json["allowCustomUserAvatars"],
        forbidRoomCreation: json["forbidRoomCreation"],
      );

  Map<String, dynamic> toJson() => {
        "allowCustomUserDisplayNames": allowCustomUserDisplayNames,
        "allowCustomUserAvatars": allowCustomUserAvatars,
        "forbidRoomCreation": forbidRoomCreation,
      };
}

class User {
  User({
    this.id,
    this.active,
    this.authType,
    this.displayName,
    this.joinedRoomIds,
    this.avatarUri,
  });

  String id;
  bool active;
  String authType;
  String displayName;
  List<String> joinedRoomIds;
  String avatarUri;

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        active: json["active"],
        authType: json["authType"],
        displayName: json["displayName"],
        joinedRoomIds: List<String>.from(json["joinedRoomIds"].map((x) => x)),
        avatarUri: json["avatarUri"] == null ? null : json["avatarUri"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "active": active,
        "authType": authType,
        "displayName": displayName,
        "joinedRoomIds": List<dynamic>.from(joinedRoomIds.map((x) => x)),
        "avatarUri": avatarUri == null ? null : avatarUri,
      };
}
