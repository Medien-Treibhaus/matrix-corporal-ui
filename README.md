# Matrix Corporal UI
<p>This is just a test for a user friendly way to read and manipulate corporal-policy.json in a matrix server with <a target="new" href="https://github.com/devture/matrix-corporal">matrix corporal</a> implemented.</p>

For now, you'll need to write your server's name and your AUTH_TOKEN in `rest_api_manager.dart`.

## Help!

This is a flutter app, since this is what I am learning write now. Being a bloody beginner I would be thankful if anybody would pick up on this!

![](https://medien-treibhaus.de/Corporal_UI.jpg)
=======

